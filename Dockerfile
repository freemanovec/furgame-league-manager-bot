FROM docker.torsten.cz/torsten-projects/common-runtimes/fgl-bot-base:3.4.0

ARG POETRY_VERSION=1.6.1
ENV POETRY_HOME=/usr/local
ENV POETRY_VIRTUALENVS_CREATE=false
ENV POETRY_INSTALLER_PARALLEL=false

RUN apt update && apt install -y curl
RUN curl -sSL https://install.python-poetry.org | python3.11 -

WORKDIR /bot
COPY poetry.lock pyproject.toml /bot/

RUN poetry install

COPY . /bot

ENTRYPOINT [ "python3.11", "-u", "-m", "fgl" ]
CMD [ "--variant", "FurgameLeagueManager" ]
