from typing import Optional


def insert_needle_into_text(
	needle: str,
	text: str,
	max_length: Optional[int] = None,
	preferred_insertion_point: Optional[str] = None,
) -> str:

	if needle.lower() in text.lower():
		return text

	if preferred_insertion_point is not None and preferred_insertion_point.lower() in text.lower():
		replacement_point: int = text.find(preferred_insertion_point)
		with_replaced: str = (
			text[:replacement_point] + needle + text[replacement_point + len(preferred_insertion_point) :]
		)
		if max_length is not None and len(with_replaced) > max_length:
			return with_replaced[:max_length]
		return with_replaced

	if max_length is not None and len(text) + len(needle) > max_length:
		return text[: -len(needle)] + needle

	return text + needle
