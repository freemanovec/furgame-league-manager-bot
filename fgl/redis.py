from typing import List, Optional
import datetime
import logging

import redis


class Redis:
	def __init__(self, redis_host: str | None) -> None:
		self._logger = logging.getLogger('redis')
		self.client = redis.Redis(redis_host) if redis_host else None  # pylint: disable=no-value-for-parameter
		self._last_confail_at: Optional[datetime.datetime] = None

	def set(self, key: str, value: Optional[str], expiration: int | None = None) -> None:
		if self.client:
			self.client.set(key.encode('utf-8'), bytes() if value is None else value.encode('utf-8'), expiration)

	def get(self, key: str, default: str) -> str:
		if not self.client:
			return default

		if self._last_confail_at is not None:
			# we had a connection failure at some point
			if self._last_confail_at + datetime.timedelta(minutes = 5) < datetime.datetime.now():
				# the connection failure is old
				self._logger.info('Connection failure expired, trying again')
				self._last_confail_at = None
			else:
				# still recent
				return default
		try:
			val: Optional[bytes] = self.client.get(key.encode('utf-8'))
			if val is None:
				self.set(key, None)
				return default
			elif len(val) == 0:
				return default
			return val.decode('utf-8')
		except Exception as e:  # pylint: disable=broad-except
			if isinstance(e, redis.ConnectionError):
				self._logger.warning('Marking redis as disconnected, will try again in 5 minutes')
				self._last_confail_at = datetime.datetime.now()
			self._logger.error('Unable to get key=%s from redis, returning default=%s (%s)', key, default, e)
			return default

	def list_keys(self) -> List[str]:
		if not self.client:
			return []

		all_keys: List[bytes] = self.client.keys()
		valid_keys: List[str] = []
		for key in all_keys:
			try:
				valid_keys.append(key.decode('utf-8'))
			except UnicodeDecodeError as e:
				self._logger.error('Encountered invalid key: %s', e)
		return valid_keys

	# helper functions

	def get_float(self, key: str, default: float | None = None) -> float:
		return float(self.get(key, str(default)))

	def get_int(self, key: str, default: int | None = None) -> int:
		return int(self.get(key, str(default)))
