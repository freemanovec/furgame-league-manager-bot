from freem_bots.random_provider import RandomProvider


class UsernameProvider:
	def __init__(self, random_provider: RandomProvider) -> None:
		self.random_provider: RandomProvider = random_provider

	def get_altered_nick(self, base: str) -> str:
		target_nickname = base
		num_chars_to_change = self.random_provider.get_int(1, (len(target_nickname) // 2) + 1)
		for _ in range(num_chars_to_change):
			random_ix = self.random_provider.get_int(0, len(target_nickname) - 1)
			target_nickname_parts = list(target_nickname)
			original_character = target_nickname[random_ix]
			new_character = original_character
			batch_a = 'eyuioaěýáíé'
			batch_a += batch_a.upper()
			batch_b = 'qwrtpsdfghjklzxcvbnmščřžď'
			batch_b += batch_b.upper()
			if original_character in batch_a:
				new_character = self.random_provider.choose_randomly(list(batch_a))
			else:
				new_character = self.random_provider.choose_randomly(list(batch_b))
			target_nickname_parts[random_ix] = new_character
			target_nickname = ''.join(target_nickname_parts)
			capitalized = target_nickname.capitalize()
			if len(target_nickname) > 1:
				target_nickname = capitalized[0] + target_nickname[1:]
			else:
				target_nickname = capitalized
		return target_nickname

	def get_altered_nick_forced_first(self, base: str) -> str:
		generated = self.get_altered_nick(base)
		return base[0] + generated[1:]

	def get_random_furry_nick_forced_first(self, base: str) -> str:
		first_character = base[0].lower()
		for _ in range(1, 200):
			generated = self.get_random_furry_nick()
			if generated.lower()[0] == first_character:
				return generated
		return self.get_random_furry_nick()

	def get_random_goodbye_message(self) -> str:
		return self.random_provider.choose_randomly(
			[
				'čus',
				'bye',
				'papá',
				'ahoj',
				'sbohem',
				'šup ven',
				'poof',
				'gone',
				'deleted',
				'removed',
				'obliterated',
				'fuč',
				'bye-b-bye',
				'cancelled',
				'yeet',
				'decimated',
			]
		)

	def get_random_furry_nick(self) -> str:
		# base code and parts courtesy of fantasynamegenerators.com

		nm11 = [
			'',
			'',
			'',
			'',
			'',
			'br',
			'bl',
			'b',
			'c',
			'ch',
			'cl',
			'cw',
			'd',
			'dr',
			'dh',
			'f',
			'h',
			'h',
			'g',
			'gl',
			'gr',
			'gw',
			'j',
			'k',
			'kh',
			'kl',
			'l',
			'm',
			'n',
			'p',
			'pr',
			'pl',
			'ph',
			'q',
			'r',
			's',
			'sh',
			'st',
			'str',
			't',
			'th',
			'tr',
			'v',
			'w',
			'z',
			'x',
			'y',
		]
		nm12 = [
			'a',
			'e',
			'i',
			'a',
			'e',
			'i',
			'a',
			'e',
			'i',
			'a',
			'e',
			'i',
			'a',
			'e',
			'i',
			'o',
			'u',
			'a',
			'e',
			'i',
			'o',
			'u',
			'a',
			'e',
			'i',
			'o',
			'u',
			'a',
			'e',
			'i',
			'o',
			'u',
			'a',
			'e',
			'i',
			'o',
			'u',
			'ai',
			'io',
			'ei',
			'ea',
			'ae',
			'ia',
			'ue',
			'ua',
		]
		nm13 = [
			'b',
			'c',
			'd',
			'f',
			'g',
			'h',
			'k',
			'l',
			'm',
			'n',
			'ph',
			'r',
			's',
			'th',
			'b',
			'c',
			'd',
			'f',
			'g',
			'h',
			'k',
			'l',
			'm',
			'n',
			'ph',
			'r',
			's',
			'th',
			'br',
			'bh',
			'b',
			'dh',
			'd',
			'dn',
			'dd',
			'f',
			'ff',
			'fr',
			'g',
			'gh',
			'gg',
			'h',
			'h',
			'hh',
			'k',
			'kr',
			'kh',
			'l',
			'll',
			'lm',
			'ln',
			'lv',
			'lw',
			'm',
			'mm',
			'mn',
			'n',
			'nc',
			'nn',
			'pp',
			'p',
			'ph',
			'pr',
			'r',
			'rg',
			'rr',
			's',
			'ss',
			'sh',
			'tt',
			'th',
			'v',
			'zn',
			'z',
		]
		nm16 = [
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'q',
			'x',
			'z',
			'ph',
			'ss',
			'sh',
			'',
			's',
			'n',
			'h',
			'l',
			'th',
		]

		parts = [
			self.random_provider.choose_randomly(nm11),
			self.random_provider.choose_randomly(nm12),
			self.random_provider.choose_randomly(nm13),
			self.random_provider.choose_randomly(nm12),
			self.random_provider.choose_randomly(nm16),
		]

		return ''.join(parts).capitalize()
