import freezegun

import fgl.welcome_prompts


@freezegun.freeze_time('2021-05-12')
def test_normal_a() -> None:
	assert len(fgl.welcome_prompts.Prompt.get_all_prompts()) > 9


@freezegun.freeze_time('2021-12-19')
def test_normal_b() -> None:
	assert len(fgl.welcome_prompts.Prompt.get_all_prompts()) > 9


@freezegun.freeze_time('2022-01-08')
def test_normal_c() -> None:
	assert len(fgl.welcome_prompts.Prompt.get_all_prompts()) > 9


@freezegun.freeze_time('2021-12-27')
def test_festive_a() -> None:
	assert len(fgl.welcome_prompts.Prompt.get_all_prompts()) == 9


@freezegun.freeze_time('2022-01-02')
def test_festive_b() -> None:
	assert len(fgl.welcome_prompts.Prompt.get_all_prompts()) == 9


@freezegun.freeze_time('2021-12-20')
def test_festive_c() -> None:
	assert len(fgl.welcome_prompts.Prompt.get_all_prompts()) == 9


@freezegun.freeze_time('2022-01-07')
def test_festive_d() -> None:
	assert len(fgl.welcome_prompts.Prompt.get_all_prompts()) == 9
