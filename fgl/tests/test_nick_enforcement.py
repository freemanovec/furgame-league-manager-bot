import fgl.mutators.nick


def test_uwu_in_nick() -> None:
	assert fgl.mutators.nick.insert_needle_into_text('UwU', 'Zukky', 32, 'u') == 'ZUwUkky'
	assert fgl.mutators.nick.insert_needle_into_text('UwU', 'Zukky', 32, 'u') != 'ZUWUkky'


def test_nick_truncation() -> None:
	assert (
		fgl.mutators.nick.insert_needle_into_text('OwO', 'AVeryLongNickThatShouldGetTruncated', 8, 'o')
		== 'AVeryLOw'
	)


def test_optional_params() -> None:
	assert (
		fgl.mutators.nick.insert_needle_into_text('OwO', 'AVeryLongNickThatShouldGetTruncated', None, 'o')
		== 'AVeryLOwOngNickThatShouldGetTruncated'
	)
	assert fgl.mutators.nick.insert_needle_into_text('OwO', 'JustANick', 32) == 'JustANickOwO'
