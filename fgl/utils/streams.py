from typing import Optional, Union
import io


class RollingBytesIOStream(io.IOBase):
	def __init__(self) -> None:
		self._rolling_queue = bytearray()
		super().__init__()

	def read(self, __size: Optional[int] = None) -> bytes:
		if __size is not None and __size < len(self._rolling_queue):
			chunk = bytes(self._rolling_queue[:__size])
			self._rolling_queue = self._rolling_queue[__size:]
		else:
			chunk = bytes(self._rolling_queue)
			self._rolling_queue = bytearray()
		return chunk

	def write(self, __buffer: Union[bytes, bytearray]) -> int:
		self._rolling_queue += __buffer
		return len(__buffer)

	def seekable(self) -> bool:
		return False

	@property
	def name(self) -> str:
		return 'rolling_bytes_io'

	@property
	def mode(self) -> str:
		return 'rolling_rw'

	@property
	def closed(self) -> bool:
		return False

	@property
	def empty(self) -> bool:
		return self.size == 0

	@property
	def size(self) -> int:
		return len(self._rolling_queue)
