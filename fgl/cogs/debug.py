import typing

import discord
import discord.cog
import discord.commands
import discord.commands.context
import freem_bots

import fgl.bots.furgame_league_manager
import fgl.cogs
import fgl.configuration


class DebugCog(fgl.cogs.Cog):
	test_guild_ids = [
		747950556630351893,
	]

	def __init__(
		self,
		bot: 'fgl.bots.furgame_league_manager.FurgameLeagueManager',
		config: 'fgl.configuration.FGLConfiguration',
	) -> None:
		self._bot = bot
		self._config = config

	@fgl.cogs.slash_command(
		description = 'Trigger a welcome interaction manually',
		guild_ids = test_guild_ids,
	)
	@discord.default_permissions(
		administrator = True,
	)
	async def debug_trigger_welcome(
		self,
		context: 'discord.commands.context.ApplicationContext',
		target_user: discord.Member,
	) -> None:
		await context.interaction.response.send_message('Proceeding..')
		await self._bot.trigger_welcome_interaction(target_user)

	@fgl.cogs.slash_command(
		description = 'Say something to user in a voice channel',
		guild_ids = test_guild_ids,
	)
	@discord.default_permissions(
		administrator = True,
	)
	async def debug_tell(
		self,
		context: 'discord.commands.context.ApplicationContext',
		target_user: discord.Member,
		text: str,
	) -> None:
		await context.interaction.response.send_message('Proceeding..')
		username = self._bot.get_safe_username_for_user(target_user)
		await self._bot.on_uwu_message(
			target_user,
			f'I have a message for {username}: {text}',
			freem_bots.TTSVoice.EN_RYAN,
		)

	@fgl.cogs.slash_command(
		description = 'Read something in many voices',
		guild_ids = test_guild_ids,
	)
	@discord.default_permissions(
		administrator = True,
	)
	async def debug_multilang(self, context: 'discord.commands.context.ApplicationContext', text: str) -> None:
		user = context.author
		if user is None:
			await context.interaction.response.send_message('Invalid user')
			return
		voiceline_parts = []
		last_voice = None
		for part in text.split(' '):
			while True:
				voice = self._bot.get_random_voice()
				if last_voice is None or last_voice.value[0] == voice.value[0]:
					last_voice = voice
					break
			voiceline_part = freem_bots.TTSVoicelinePart(voice, part)
			voiceline_parts.append(voiceline_part)
		voiceline = freem_bots.TTSVoiceline(
			voiceline_parts,
			effects = self._config.voice_effects,
		)
		channel = self._bot.locate_user_in_voice_channel(typing.cast(discord.User, user))
		if channel is None:
			await context.interaction.response.send_message('User not in any channel')
			return
		await context.interaction.response.send_message('Proceeding..')
		pcm = await self._bot.get_voiceline_pcm(voiceline)
		await self._bot.play_pcms_in_voice_channel(channel, [pcm])

	@fgl.cogs.slash_command(
		description = "Change a user's nickname",
		guild_ids = test_guild_ids,
	)
	@discord.default_permissions(
		administrator = True,
	)
	async def debug_rename(
		self,
		context: 'discord.commands.context.ApplicationContext',
		target_user: discord.Member,
		nickname: str,
	) -> None:
		try:
			await target_user.edit(nick = nickname)
			await context.interaction.response.send_message('Done 👌')
		except Exception as e:  # pylint: disable=broad-except
			await context.interaction.response.send_message(f'Failed: {e}')
