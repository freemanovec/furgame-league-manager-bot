import discord
import discord.cog
import discord.commands
import discord.commands.context

import fgl.bots.furgame_league_manager
import fgl.cogs


class ConfigurationCog(fgl.cogs.Cog):
	test_guild_ids = [
		747950556630351893,
	]

	def __init__(self, bot: 'fgl.bots.furgame_league_manager.FurgameLeagueManager') -> None:
		self.bot = bot

	@fgl.cogs.slash_command(
		description = 'Write a setting',
		guild_ids = test_guild_ids,
	)
	@discord.default_permissions(
		administrator = True,
	)
	async def settings_write(
		self,
		context: 'discord.commands.context.ApplicationContext',
		key: str,
		value: str,
		expiration: str | None = None,
	) -> None:
		try:
			self.bot.redis.set(key, value, None if expiration is None else int(expiration))
			await context.interaction.response.send_message('👌 Set')
		except Exception as e:  # pylint: disable=broad-except
			await context.interaction.response.send_message(f'🚨 Error: {e}')

	@fgl.cogs.slash_command(
		description = 'Read a setting',
		guild_ids = test_guild_ids,
	)
	@discord.default_permissions(
		administrator = True,
	)
	async def settings_read(self, context: 'discord.commands.context.ApplicationContext', key: str) -> None:
		try:
			value = self.bot.redis.get(key, default = '🚨 MISSING 🚨')
			await context.interaction.response.send_message(f'{key=}: {value=}')
		except Exception as e:  # pylint: disable=broad-except
			await context.interaction.response.send_message(f'🚨 Error: {e}')

	@fgl.cogs.slash_command(
		description = 'List all settings and their values',
		guild_ids = test_guild_ids,
	)
	@discord.default_permissions(
		administrator = True,
	)
	async def settings_list(self, context: 'discord.commands.context.ApplicationContext') -> None:
		step_size = 10
		try:
			keys = sorted(self.bot.redis.list_keys())
			if len(keys) == 0:
				await context.interaction.response.send_message('Empty 😞')
			else:
				await context.interaction.response.send_message(f'{len(keys)} keys present')
				for i in range(0, len(keys), step_size):
					key_range = keys[i : min(i + step_size, len(keys))]
					lines = []
					for key in key_range:
						value = self.bot.redis.get(key, default = 'Nothing')
						lines.append(f'`{key}`: **{value}**')
					msg = '\n'.join(lines)
					await context.interaction.followup.send(msg)
		except Exception as e:  # pylint: disable=broad-except
			await context.interaction.response.send_message(f'🚨 Error: {e}')
