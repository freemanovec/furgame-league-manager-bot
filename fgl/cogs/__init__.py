from typing import Callable, List, Optional
import typing

import discord.cog
import discord.commands


def message_command(
	name: str,
	description: str,
	guild_ids: Optional[List[int]],
) -> Callable[..., discord.commands.MessageCommand]:
	return typing.cast(
		Callable[..., discord.commands.MessageCommand],
		discord.commands.message_command(name = name, description = description, guild_ids = guild_ids),  # type: ignore
	)


def slash_command(
	description: str,
	guild_ids: Optional[List[int]],
) -> Callable[..., discord.commands.SlashCommand]:
	return typing.cast(
		Callable[..., discord.commands.SlashCommand],
		discord.commands.slash_command(description = description, guild_ids = guild_ids),  # type: ignore
	)


class Cog(discord.cog.Cog):
	pass
