from typing import Any, Callable, Coroutine, List
import os

from discord.channel import VoiceChannel
import discord
import discord.cog
import discord.commands
import discord.commands.context
import discord.enums
import freem_bots

import fgl.bots.furgame_league_manager
import fgl.cogs


async def _get_audio_options(_: discord.AutocompleteContext) -> List[str]:
	directory_listing = os.listdir('audio_files/')
	return [filename.removesuffix('.wav') for filename in directory_listing if '.wav' in filename]


_play_audio_autocomplete = discord.commands.Option(
	str,
	'Pick the audiofile',
	autocomplete = _get_audio_options,
)


class TTSCog(fgl.cogs.Cog):
	test_guild_ids = [
		747950556630351893,
	]

	def __init__(self, bot: 'fgl.bots.furgame_league_manager.FurgameLeagueManager') -> None:
		self.bot = bot

		self._dyn_init_slashes()

	def _dyn_init_slashes(self) -> None:
		voice_definitions = [
			('Ryan', freem_bots.TTSVoice.EN_RYAN),
			('William', freem_bots.TTSVoice.EN_WILLIAM),
			('Clara', freem_bots.TTSVoice.EN_CLARA),
			('Neerja', freem_bots.TTSVoice.EN_NEERJA),
			('Antonin', freem_bots.TTSVoice.CS_ANTONIN),
			('Vlasta', freem_bots.TTSVoice.CS_VLASTA),
			('Keita', freem_bots.TTSVoice.JA_KEITA),
			('Nanami', freem_bots.TTSVoice.JA_NANAMI),
			('Agnieszka', freem_bots.TTSVoice.PL_AGNIESZKA),
			('Svetlana', freem_bots.TTSVoice.RU_SVETLANA),
			('Ada', freem_bots.TTSVoice.EN_ADA),
			('Xenoworx', freem_bots.TTSVoice.EN_XENOWORX),
		]

		def rebind(
			voice: freem_bots.TTSVoice,
		) -> Callable[[TTSCog, discord.ApplicationContext, str], Coroutine[Any, Any, None]]:
			async def cur_com(
				self: TTSCog,
				context: 'discord.commands.context.ApplicationContext',
				text: str,
			) -> None:
				await self._say_voice(context, text, voice)

			return cur_com

		for (voice_name, voice) in voice_definitions:
			rebound = rebind(voice)

			generated_command = discord.commands.SlashCommand(
				func = rebound,
				name = f'say_{voice_name.lower()}',
				description = f'Says the text you provide using the voice of {voice_name}',
				guild_ids = TTSCog.test_guild_ids,
			)
			options = [
				discord.commands.Option(
					discord.enums.SlashCommandOptionType.string,
					name = 'text',
					description = 'What to say',
				)
			]
			for option in options:
				# pylint: disable=protected-access
				option._parameter_name = option.name
			generated_command.options = options

			self._add_command(generated_command)

	def _add_command(self, command: discord.commands.SlashCommand) -> None:
		# pylint: disable=attribute-defined-outside-init
		self.__cog_commands__ = self.__cog_commands__ + (command,)  # type: ignore

	@fgl.cogs.message_command(
		name = 'Read (William)',
		description = 'Reads this message in your current voice channel as William',
		guild_ids = test_guild_ids,
	)
	async def read_message_william(
		self,
		context: 'discord.commands.context.ApplicationContext',
		message: 'discord.Message',
	) -> None:
		await self._say_voice(context, message.content, freem_bots.TTSVoice.EN_WILLIAM)

	@fgl.cogs.message_command(
		name = 'Read (Vlasta)',
		description = 'Reads this message in your current voice channel as Vlasta',
		guild_ids = test_guild_ids,
	)
	async def read_message_vlasta(
		self,
		context: 'discord.commands.context.ApplicationContext',
		message: 'discord.Message',
	) -> None:
		await self._say_voice(context, message.content, freem_bots.TTSVoice.CS_VLASTA)

	@fgl.cogs.message_command(
		name = 'Read (Jakub)',
		description = 'Reads this message in your current voice channel as Jakub',
		guild_ids = test_guild_ids,
	)
	async def read_message_jakub(
		self,
		context: 'discord.commands.context.ApplicationContext',
		message: 'discord.Message',
	) -> None:
		await self._say_voice(context, message.content, freem_bots.TTSVoice.CS_JAKUB)

	@fgl.cogs.slash_command(
		description = 'Plays an audio file (must be baked in)',
		guild_ids = test_guild_ids,
	)
	async def play_audio(
		self,
		context: 'discord.commands.context.ApplicationContext',
		audiofile: _play_audio_autocomplete,  # type: ignore
	) -> None:
		if isinstance(context.author, discord.Member):
			member: discord.Member = context.author
			if member.voice is not None and isinstance(member.voice.channel, VoiceChannel):
				voice_channel: VoiceChannel = member.voice.channel
				await context.interaction.response.send_message('Going to play it now 💹')
				await self.bot.play_audiofile(audiofile, voice_channel, self.bot.is_continuous_mode_enabled)
			else:
				await context.interaction.response.send_message('You are not in a voice channel')
		else:
			await context.interaction.response.send_message("You don't have any voice channel associated")

	async def _say_voice(
		self,
		context: 'discord.commands.context.ApplicationContext',
		text: str,
		voice: freem_bots.TTSVoice,
	) -> None:
		if isinstance(context.author, discord.Member):
			member: discord.Member = context.author
			if member.voice is not None and member.voice.channel is not None and context.guild is not None:
				await context.interaction.response.send_message(f'Saying `{text}` in `{member.voice.channel.name}`')
				await self.bot.on_uwu_message(context.author, text, force_voice = voice)
			else:
				await context.interaction.response.send_message('Invalid target')
		else:
			await context.interaction.response.send_message("You don't have any voice channel associated")
