from typing import List, Optional, Tuple, TypeVar
import asyncio
import io
import typing

from discord.channel import VoiceChannel
from freem_bots import DiscordBot, TTSVoice
import discord
import freem_bots

from fgl.utils.streams import RollingBytesIOStream


class CommonBot(DiscordBot):  # pylint: disable=abstract-method
	def __init__(self, configuration: freem_bots.Configuration) -> None:
		super().__init__(configuration)

		self.configuration = configuration

	async def on_ready(self) -> None:
		''' Invoked when bot connects to Discord servers '''
		await super().on_ready()

	async def get_voice_channel_with_owner(self) -> Optional[VoiceChannel]:
		owner_id = 176557902910193668
		owner = self.get_user(owner_id)
		if owner is None:
			return None
		channel = self.locate_user_in_voice_channel(owner)
		return channel

	async def stream_from_rolling_stream(
		self,
		guild: discord.Guild,
		stream: RollingBytesIOStream,
		cancellation_event: asyncio.Event,
	) -> None:
		while stream.empty and not cancellation_event.is_set():
			await asyncio.sleep(0.1)
		async with self._get_voice_lock_for_guild(guild):
			if guild.id in self._active_voice_clients:
				# we are indeed connected somewhere in this guild
				voice_client = self._active_voice_clients[guild.id]
				audio_source = discord.PCMAudio(typing.cast(io.BufferedIOBase, stream))
				self._logger.debug('Starting streaming')
				voice_client.play(audio_source)
				while voice_client.is_playing():
					if cancellation_event.is_set():
						voice_client.stop()
					await asyncio.sleep(0.05)
				self._logger.debug('Done streaming')
			else:
				# we are not connected in this guild
				self._logger.error('Requested to stream audio in a guild where we are not in a voice channel')

	def get_weighted_random_voice(self) -> TTSVoice:
		''' Gets a voice name from a weighted set '''
		possible_voices = [
			(
				TTSVoice.EN_RYAN,
				30.0,
			),  # my favorite, good volume, great pronunciation, could be sped up a bit
		]
		return self.__get_random_weighted(possible_voices)

	def get_random_voice(self) -> TTSVoice:
		''' Gets a completely random voice '''
		return self._random_provider.choose_randomly(list(TTSVoice))

	_tvP = TypeVar('_tvP')

	def __get_random_weighted(self, possibilities: List[Tuple[_tvP, float]]) -> _tvP:
		probability_matrix = []
		subtotal = 0.0
		for possibility in possibilities:
			probability_matrix.append(subtotal)
			subtotal += possibility[1]
		maximum_value = probability_matrix[-1] + possibilities[-1][1]
		random_value = self._random_provider.get_float() * maximum_value
		hit_index = 0
		for i, current_value in enumerate(probability_matrix):
			next_value = maximum_value if i == (len(probability_matrix) - 1) else probability_matrix[i + 1]
			current_value = probability_matrix[i]
			if current_value <= random_value <= next_value:
				hit_index = i
				break
		selected = possibilities[hit_index][0]
		return selected

	def is_superuser(self, identifier: int) -> bool:
		return identifier in [
			176557902910193668,  # Freem
			326449069864255488,  # Darxandri
			332298141028057089,  # Arixen
			207914979389603840,  # Zukkys
		]

	@property
	def is_continuous_mode_enabled(self) -> bool:
		return False
