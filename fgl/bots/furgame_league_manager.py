from typing import Callable, Dict, List, Optional, Union
import asyncio
import datetime
import re
import typing

from discord.member import Member, VoiceState
from discord.message import Message
from discord.user import User
from freem_bots import TTSVoice, TTSVoiceline, TTSVoicelinePart, TTSVoicelineSpeed
import discord
import openai
import requests

from fgl.bots import CommonBot
from fgl.cogs.configuration import ConfigurationCog
from fgl.cogs.debug import DebugCog
from fgl.cogs.tts import TTSCog
from fgl.redis import Redis
import fgl.configuration
import fgl.mutators.nick
import fgl.username_provider
import fgl.welcome_prompts


class FurgameLeagueManager(CommonBot):  # pylint: disable=abstract-method
	def __init__(self) -> None:
		self.top_config = fgl.configuration.FGLConfiguration()
		super().__init__(self.top_config)

		self.redis: Redis = Redis(self.top_config.redis_hostname)

		self.user_heartrate_dict: Dict[int, int] = {}
		self.username_provider = fgl.username_provider.UsernameProvider(self._random_provider)
		self.voice_queue: 'asyncio.Queue[str]' = asyncio.Queue()

		self.emoji_pattern = re.compile(
			'['
			'\U0001F600-\U0001F64F'  # emoticons
			'\U0001F300-\U0001F5FF'  # symbols & pictographs
			'\U0001F680-\U0001F6FF'  # transport & map symbols
			'\U0001F1E0-\U0001F1FF'  # flags (iOS)
			'\U00002702-\U000027B0'
			'\U000024C2-\U0001F251'
			'\U0001f926-\U0001f937'
			'\U0001F1F2'
			'\U0001F1F4'
			'\U0001F620'
			'\u200d'
			'\u2640-\u2642'
			'\u2600-\u2B55'
			'\u23cf'
			'\u23e9'
			'\u231a'
			'\ufe0f'  # dingbats
			'\u3030'
			'\U00002500-\U00002BEF'  # Chinese char
			'\U00010000-\U0010ffff'
			']+',
			flags = re.UNICODE,
		)

		if self.top_config.enable_slash_commands:
			for cog in [
				TTSCog(self),
				ConfigurationCog(self),
				DebugCog(self, self.top_config),
			]:
				self.add_cog(cog)

		if not self.top_config.testing_mode:
			self._init_tasks += [
				# self.task_enforce_nick(
				# 	211823830488842241,
				# 	'Radar',
				# 	lambda inp: 'rAco ' + (self.username_provider.get_random_goodbye_message()),
				# ),
				# self.task_enforce_nick(
				# 	230335875953852416,
				# 	'Oggy',
				# 	lambda inp: 'oAco ' + (self.username_provider.get_random_goodbye_message()),
				# ),
				# self.task_countdown_nick(
				# 	user_id = 332298141028057089,  # Arixen
				# 	prefix = 'T: ',
				# 	end_time = datetime.datetime(2023, 7, 12, 15, 0, 0, tzinfo = datetime.timezone.utc),
				# ),
				# self.task_enforce_nick(
				# 	326449069864255488,
				# 	'Darx',
				# 	lambda inp: 'dAco ' + (self.username_provider.get_random_goodbye_message()),
				# ),
				# self.task_countdown_nick(
				# 	176557902910193668,  # Freem
				# 	datetime.datetime(2023, 2, 24, 14, 0, 0, tzinfo = datetime.timezone.utc),
				# ),
				# self.task_enforce_nick(
				# 	306117896038252546,
				# 	'Meepo',
				# 	lambda inp: 'mAco ' + (self.username_provider.get_random_goodbye_message()),
				# ),
				# self.task_enforce_nick(
				# 	207914979389603840,
				# 	'Zukky',
				# 	lambda inp: 'zAco ' + (self.username_provider.get_random_goodbye_message()),
				# ),
				self.task_log_to_voice_channel(),
			]

	async def task_log_to_voice_channel(self) -> None:
		while not self.is_closed():
			messages = []
			while not self.voice_queue.empty():
				cur_message: str = await self.voice_queue.get()
				messages.append(cur_message)
			if len(messages) > 0:
				message = '. '.join(messages)
				if len(message) > 2000:
					message = message[-2000:]
				self._logger.info('Voice log message: %s', message)
				if self.redis.get('bot_voice_log_enabled', 'false') == 'true':
					vc = await self.get_voice_channel_with_owner()
					if vc is None:
						self._logger.info('Owner not found in a voice channel')
					else:
						await self.play_voiceline_in_voice_channel(
							vc,
							TTSVoiceline(
								[TTSVoicelinePart(TTSVoice.EN_WILLIAM, message, speed = TTSVoicelineSpeed.FAST)],
								effects = self.top_config.voice_effects,
							),
							stay_connected = self.is_continuous_mode_enabled,
						)
				else:
					self._logger.info('Voice logging disabled via configuration, dropping message')
			await asyncio.sleep(3.0)

	async def task_enforce_nick(
		self,
		user_id: int,
		nickname: str,
		username_provider_function: Callable[[str], str] | None = None,
	) -> None:
		await self.wait_until_ready()
		await asyncio.sleep(
			self._random_provider.get_float() * self.redis.get_float('bot_throttling_nick_modification_max_sleep', 10)
		)

		while not self.is_closed():
			try:
				target_nickname = nickname
				if username_provider_function is not None:
					target_nickname = username_provider_function(target_nickname)
				target_user = await self._get_user_by_uid('Furgame league', user_id)
				if target_user is None:
					await asyncio.sleep(5)
					continue
				# check nickname
				if target_user.nick != target_nickname:
					# nicknames differ
					self._logger.debug('Going to change nickname')
					await target_user.edit(nick = target_nickname)
					self._logger.info(
						"Nickname of user '%s' changed to '%s'",
						target_user,
						target_nickname,
					)  # TODO get user name + discriminator
				# sleep for 5 seconds
				await asyncio.sleep(
					self.redis.get_float('bot_throttling_nick_modification_between_sleep_base', 8)
					+ (
						self._random_provider.get_float()
						* self.redis.get_float('bot_throttling_nick_modification_between_sleep_max', 5)
					)
				)
			except discord.DiscordException as e:
				self._logger.error(e)
				await asyncio.sleep(self.redis.get_float('bot_throttling_nick_modification_exception_sleep', 5))

	async def task_countdown_nick(self, user_id: int, prefix: str, end_time: datetime.datetime) -> None:
		await self.wait_until_ready()
		await asyncio.sleep(
			self._random_provider.get_float() * self.redis.get_float('bot_throttling_nick_modification_max_sleep', 10)
		)

		while not self.is_closed():
			try:
				current_time = datetime.datetime.now(tz = datetime.timezone.utc)
				remaining_timedelta = end_time - current_time
				remaining_timedelta_total_seconds = int(remaining_timedelta.total_seconds())
				remaining_timedelta_days = abs(remaining_timedelta_total_seconds) // (3600 * 24)
				remaining_timedelta_hours = abs(remaining_timedelta_total_seconds) % (3600 * 24) // 3600
				remaining_timedelta_minutes = abs(remaining_timedelta_total_seconds) % 3600 // 60
				remaining_timedelta_seconds = abs(remaining_timedelta_total_seconds) % 60

				if remaining_timedelta_days > 1:
					remaining_timedelta_string_positive = f'{remaining_timedelta_days} days'
				else:
					remaining_timedelta_string_positive = (
						f'{remaining_timedelta_hours:02}:{remaining_timedelta_minutes:02}:{remaining_timedelta_seconds:02}'
					)

				target_nickname = (
					prefix + ('-' if remaining_timedelta_total_seconds >= 0 else '+') + remaining_timedelta_string_positive
				)
				target_user = await self._get_user_by_uid('Furgame league', user_id)
				if target_user is None:
					await asyncio.sleep(5)
					continue
				# check nickname
				if target_user.nick != target_nickname:
					# nicknames differ
					self._logger.debug('Going to change nickname')
					await target_user.edit(nick = target_nickname)
				# sleep for 5 seconds
				await asyncio.sleep(
					self.redis.get_float('bot_throttling_nick_modification_between_sleep_base', 8)
					+ (
						self._random_provider.get_float()
						* self.redis.get_float('bot_throttling_nick_modification_between_sleep_max', 5)
					)
				)
			except discord.DiscordException as e:
				self._logger.error(e)
				await asyncio.sleep(self.redis.get_float('bot_throttling_nick_modification_exception_sleep', 5))

	async def task_enforce_nick_contains(self, user_id: int, needle: str, insertion_point: str) -> None:
		await self.wait_until_ready()
		await asyncio.sleep(
			self._random_provider.get_float() * self.redis.get_float('bot_throttling_nick_modification_max_sleep', 10)
		)

		while not self.is_closed():
			try:
				target_user = await self._get_user_by_uid('Furgame league', user_id)
				if target_user is None:
					await asyncio.sleep(5)
					continue
				current_nickname = target_user.nick
				if current_nickname is None:
					current_nickname = 'Empty'
				max_length = self.redis.get_int(
					'validation_nick_modification_appending_max_length',
					31,
				)
				target_nickname = fgl.mutators.nick.insert_needle_into_text(
					needle,
					current_nickname,
					max_length,
					insertion_point,
				)
				if current_nickname != target_nickname:
					self._logger.debug('Going to change nickname')
					await target_user.edit(nick = target_nickname)
					self._logger.info(
						"Nickname of user '%s' changed to '%s'",
						target_user,
						target_nickname,
					)  # TODO get user name + discriminator

				# sleep for 5 seconds
				await asyncio.sleep(
					self.redis.get_float('bot_throttling_nick_modification_between_sleep_base', 8)
					+ (
						self._random_provider.get_float()
						* self.redis.get_float('bot_throttling_nick_modification_between_sleep_max', 5)
					)
				)
			except discord.DiscordException as e:
				self._logger.error(e)
				await asyncio.sleep(self.redis.get_float('bot_throttling_nick_modification_exception_sleep', 5))

	async def task_add_heartbeat_to_nick(self, user_id: int, base_nick: str) -> None:
		base_nick = base_nick.strip()
		last_nick = None
		still_for_iterations = 0
		while not self.is_closed():
			try:
				target_user = await self._get_user_by_uid('Furgame league', user_id)
				if target_user is None:
					await asyncio.sleep(self.redis.get_float('bot_throttling_nick_modification_exception_sleep', 5))
					continue
				try:
					if user_id not in self.user_heartrate_dict:
						await asyncio.sleep(self.redis.get_float('bot_throttling_nick_modification_exception_sleep', 5))
						continue
					heartrate = self.user_heartrate_dict[user_id]
					self._logger.debug('Will set new heartrate: %s', heartrate)
					if heartrate < self.redis.get_int('bot_heartrate_monitoring_bpm_threshold_sleep', 68):
						surrounding_emoji = '💤'
					elif heartrate > self.redis.get_int('bot_heartrate_monitoring_bpm_threshold_anger', 95):
						surrounding_emoji = '💢'
					else:
						surrounding_emoji = ''
					target_nickname = f'{surrounding_emoji}{base_nick}{surrounding_emoji} - {heartrate} BPM'
					if still_for_iterations > self.redis.get_int('bot_heartrate_monitoring_iteration_threshold_died', 8):
						# heart rate too still, might've died? set nickname to base
						target_nickname = base_nick
						last_nick = target_nickname
						await target_user.edit(nick = target_nickname)
					else:
						if target_nickname != last_nick:
							last_nick = target_nickname
							still_for_iterations = 0
							self._logger.debug('Going to change nickname')
							await target_user.edit(nick = target_nickname)
							self._logger.info(
								"Nickname of user '%s' changed to '%s'",
								target_user,
								target_nickname,
							)  # TODO get user name + discriminator
						else:
							still_for_iterations += 1
				except discord.DiscordException:
					self._logger.error('Error setting heartrate nick')
					await target_user.edit(nick = base_nick)  # websocket connection closed or other error
				await asyncio.sleep(self.redis.get_float('bot_throttling_nick_modification_heartbeat_between_sleep', 15))
			except discord.DiscordException:
				await asyncio.sleep(
					self.redis.get_float('bot_throttling_nick_modification_heartbeat_exception_sleep', 30)
				)

	@staticmethod
	def preprocess_username(username: str) -> str:
		''' Rewrites username for additional functionality and pronunciation '''
		character_replacement_dict = {'č': 'ch', 'š': 'sh'}
		for (original_char, target_string) in character_replacement_dict.items():
			username = username.replace(original_char, target_string)
		username = username.replace('CMDR', 'Commander')
		return username

	def get_safe_username_for_user(self, user: discord.Member) -> str:
		username = user.nick if user.nick is not None else user.name
		username = FurgameLeagueManager.preprocess_username(username)
		return username

	async def on_message(self, message: Message) -> None:
		''' Invoked on any new message in any channel '''
		author = message.author
		if not isinstance(author, Member):
			return
		if author is None or self.user is None:
			return
		if author.id == self.user.id:
			return
		clean_content = message.clean_content
		if clean_content.startswith('-heck') and self.is_superuser(author.id):
			await self.on_random_gpt_prompt_message(message, message.clean_content[5:], TTSVoice.EN_JENNY)
		elif clean_content == '-debug':
			await self.trigger_welcome_interaction(author)

	async def on_uwu_message(
		self,
		author: Member,
		content: str,
		force_voice: Optional[TTSVoice] = None,
	) -> None:
		content = content.replace('\n', '. ').strip()

		# locate member in voice channels
		located_channel = self.locate_user_in_voice_channel(typing.cast(User, author))
		if located_channel is None:
			return

		if len(content) > 200 and author.id != 176557902910193668:
			content = "Look how they're trying to massacre my API"

		voice: TTSVoice = self.get_weighted_random_voice() if force_voice is None else force_voice
		voiceline = TTSVoiceline(
			[TTSVoicelinePart(voice, content)],
			effects = self.top_config.voice_effects,
		)
		await self.play_voiceline_in_voice_channel(
			located_channel,
			voiceline,
			stay_connected = self.is_continuous_mode_enabled,
		)

	async def on_random_gpt_prompt_message(
		self,
		message: Message,
		prompt: str,
		force_voice: Optional[TTSVoice] = None,
	) -> None:
		prompt = prompt.strip()

		# locate member in voice channels
		if message.guild is None:
			self._logger.warning('No guild associated with message!')
			return
		located_channel = self.locate_user_in_voice_channel(typing.cast(User, message.author))
		if located_channel is None:
			return

		voice = self.get_weighted_random_voice() if force_voice is None else force_voice
		if len(prompt) > 200 and message.author.id != 176557902910193668:
			content = "Look how they're trying to massacre my API"
		else:
			gpt_response = await self.get_gpt_simple(prompt)
			if gpt_response is None:
				self._logger.error('Empty GPT response!')
				return
			content = prompt.strip() + ' ' + gpt_response.strip()
			self._logger.info("GPT-4 prompt '%s' - response: '%s'", prompt, content)
		voiceline = TTSVoiceline(
			[TTSVoicelinePart(voice, content)],
			effects = self.top_config.voice_effects,
		)
		await self.play_voiceline_in_voice_channel(
			located_channel,
			voiceline,
			stay_connected = self.is_continuous_mode_enabled,
		)

	async def on_voice_state_update(self, member: discord.Member, before: VoiceState, after: VoiceState) -> None:
		''' Invoked when a voice state of any member updates '''
		await super().on_voice_state_update(member, before, after)

		def log_update(state_name: str) -> None:
			self._logger.info('Voice state update (%s) for %s (%s)', state_name, member.name, member.id)

		if self.user is None or member is None:
			return

		if self.user.id == member.id:
			# is a change on self
			if before.channel is None and after.channel is not None:
				log_update('Self-connect')
				return  # just connected, this is normal
			elif before.channel is not None and after.channel is None:
				log_update('Self-disconnect')
				return  # disconnected, might be forced, but this is allowed
			else:
				log_update('Self-misc')
				return
		# if(member.bot):
		#    return
		if before.channel == after.channel:
			log_update('Same-channel misc')
			return
		if before.channel is not None and after.channel is not None:
			log_update('Move between channels')
			return
		if before.channel is not None and after.channel is None:
			log_update('Disconnect')
			return
		if before.channel is None and after.channel is not None:
			log_update('Connect')
			# if(len(after.channel.members) > 5):
			#    # channel busy, don't join
			#    return
			await self.trigger_welcome_interaction(member)
			return
		log_update('Unhandled')

	async def generate_user_prompt(self, target_user: discord.Member) -> bytes:
		username = self.get_safe_username_for_user(target_user)
		username = username.lstrip(':')

		possible_prompts: List[
			Union[fgl.welcome_prompts.TextPrompt, fgl.welcome_prompts.AudiofilePrompt]
		] = fgl.welcome_prompts.Prompt.get_usable_prompts()
		for possible_prompt in possible_prompts:
			cur_probability = 5.0
			possible_prompt.probability = cur_probability

		voice = self.get_weighted_random_voice()
		skip_reason: Optional[str] = None
		prompt: Optional[Union[fgl.welcome_prompts.TextPrompt, fgl.welcome_prompts.AudiofilePrompt]]
		if await self._should_use_legacy_prompt(target_user):
			skip_reason = 'Forced legacy'
			prompt = None
		elif self._random_provider.get_float() > (
			1 - self.redis.get_float('bot_welcoming_tts_probabilities_use_gpt', 0.8)
		):
			gpt_seeds = fgl.welcome_prompts.Prompt.get_seed_prompts()
			prompt = await self.get_gpt_prompt(gpt_seeds)
			if prompt is None:
				skip_reason = 'No response from GPT engine'
				prompt = None
			else:
				self._logger.info('GPT-3 generated message: %s', prompt)
				if '{}' not in prompt.prompt:
					skip_reason = 'Missing username token'
					prompt = None
		else:
			skip_reason = 'Probability skip'
			prompt = None

		if prompt == None:
			self._logger.info("Won't use GPT-3, reason: %s", skip_reason)
			prompt = fgl.welcome_prompts.Prompt.get_random_weighted(possible_prompts, self._random_provider)
			self._logger.info('Legacy-style generated message: %s', prompt)

		pcms = []
		if isinstance(prompt, fgl.welcome_prompts.TextPrompt):
			pcms.append(
				await self.get_audio_for_text(prompt.prompt, voice, username, effects = self.top_config.voice_effects)
			)
		elif isinstance(prompt, fgl.welcome_prompts.AudiofilePrompt):
			full_path = f'audio_files/{prompt.filename}.wav'
			pcms.append(self.get_pcm_from_file(full_path))
			pcms.append(await self.get_audio_for_text('{}', voice, username, effects = self.top_config.voice_effects))

		return b''.join(pcms)

	async def trigger_welcome_interaction(self, target_user: discord.Member) -> None:
		# check for ready prompt in cache
		# if exists
		#  play the prompt, then generate new one and save to cache
		# if doesn't exist
		#  generate a prompt, then play it, then generate new one and save to cache

		voice_channel = self.locate_user_in_voice_channel(typing.cast(User, target_user))
		if voice_channel is None:
			self._logger.warning('No voice channel for user')
			return

		if self._cache is None:
			pcm = await self.generate_user_prompt(target_user)
			await self.play_pcms_in_voice_channel(
				voice_channel,
				[pcm],
				stay_connected = self.is_continuous_mode_enabled,
			)
			return

		ready_prompt_cache_key = f'ready_prompt_{target_user.id}'
		cached_ready_prompt = await self._cache.get(ready_prompt_cache_key)
		if not cached_ready_prompt:
			# no prompt exists, generate a new one
			cached_ready_prompt = await self.generate_user_prompt(target_user)

		# play the prompt
		await self.play_pcms_in_voice_channel(
			voice_channel,
			[cached_ready_prompt],
			stay_connected = self.is_continuous_mode_enabled,
		)

		# generate a new prompt
		new_ready_prompt = await self.generate_user_prompt(target_user)

		# save it to cache
		await self._cache.set(ready_prompt_cache_key, new_ready_prompt)

	async def _should_use_legacy_prompt(self, member: Union[Member, User]) -> bool:
		try:
			legacy_user_ids: List[int] = [
				# 332298141028057089,  # Arixen
			]
			if member.id in legacy_user_ids:
				return True
		except discord.DiscordException as e:
			self._logger.error(e)
		return False

	async def get_gpt_prompt(self, seed_prompts_l: list[str]) -> Optional[fgl.welcome_prompts.TextPrompt]:
		openai.api_key = self.top_config.openai_token

		prompt_username = 'Jeff'
		seed_prompts_l = [i.replace('{}', prompt_username) for i in seed_prompts_l]
		seed_prompts = '\n'.join(seed_prompts_l)
		seed_prompts = f'{seed_prompts}\n'

		try:
			response = openai.Completion.create(  # type: ignore
				model = 'davinci-002',
				prompt = seed_prompts,
				temperature = self.redis.get_float('bot_generation_gpt_tuning_temperature', 1),
				max_tokens = self.redis.get_int('bot_generation_gpt_tuning_max_tokens', 40),
				top_p = self.redis.get_float('bot_generation_gpt_tuning_top_p', 1),
				frequency_penalty = self.redis.get_float('bot_generation_gpt_tuning_penalty_frequency', 1),
				presence_penalty = self.redis.get_float('bot_generation_gpt_tuning_penalty_presence', 1),
				stop = ['\n'],
				timeout = 4.0,
			)
			response = response.choices[0]['text']
			response = response.replace(prompt_username, '{}')
			response = self.emoji_pattern.sub('', response)
			return fgl.welcome_prompts.TextPrompt(response)
		except requests.exceptions.RequestException:
			return None
		except Exception as e:  # pylint:disable=broad-except
			self._logger.warning('OpenAI crashed with %s', e)
			return None

	async def get_gpt_simple(self, prompt: str) -> Optional[str]:
		if len(prompt) == 0:
			return None
		if prompt[-1] == '\n':
			prompt = prompt[:-1]

		openai.api_key = self.top_config.openai_token

		try:
			response = openai.Completion.create(  # type: ignore
				engine = self.redis.get('bot_generation_gpt_tuning_simple_engine', 'davinci'),
				prompt = prompt,
				temperature = self.redis.get_float('bot_generation_gpt_tuning_temperature', 1),
				max_tokens = self.redis.get_int('bot_generation_gpt_tuning_max_tokens', 40),
				top_p = self.redis.get_float('bot_generation_gpt_tuning_top_p', 1),
				frequency_penalty = self.redis.get_float('bot_generation_gpt_tuning_penalty_frequency', 1),
				presence_penalty = self.redis.get_float('bot_generation_gpt_tuning_penalty_presence', 1),
				stop = ['\n'],
				timeout = 4.0,
			)
			response = response['choices'][0]['text']
			return typing.cast(Optional[str], response)
		except requests.exceptions.RequestException:
			return None
		except Exception as e:  # pylint:disable=broad-except
			self._logger.warning('OpenAI crashed with %s', e)
			return None

	@property
	def is_continuous_mode_enabled(self) -> bool:
		return self.redis.get('bot_continuous_mode', 'false').lower() == 'true'
