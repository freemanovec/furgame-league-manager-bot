from typing import Dict, TypeVar
import logging

from freem_bots import Configuration
import click
import openai

from fgl.bots import CommonBot
from fgl.bots.furgame_league_manager import FurgameLeagueManager


@click.command(name = 'run')
@click.option(
	'--variant',
	type = click.Choice(['FurgameLeagueManager'], case_sensitive = False),
	default = 'FurgameLeagueManager',
)
def main(variant: str) -> None:
	logging.basicConfig()
	logging.root.setLevel(logging.INFO)
	logging.getLogger('discord').setLevel(logging.WARNING)
	logging.getLogger('websockets.client').setLevel(logging.WARNING)
	openai.log = logging.getLogger('openai')  # type: ignore
	openai.log.setLevel(logging.WARN)  # type: ignore

	tVarBot = TypeVar('tVarBot', bound = CommonBot)
	bot_mapping: Dict[str, tVarBot] = {  # type: ignore
		'FurgameLeagueManager': FurgameLeagueManager,
	}
	if variant not in bot_mapping:
		raise Exception('Invalid variant specified')
	configuration = Configuration()
	manager: CommonBot = bot_mapping[variant]()  # type: ignore
	manager.run(configuration.discord_token)


if __name__ == '__main__':
	main()  # pylint: disable=no-value-for-parameter
