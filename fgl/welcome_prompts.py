from abc import abstractmethod  # pylint: disable=no-name-in-module
from typing import List, Optional, Union
import datetime
import os

import freem_bots.random_provider


class Prompt:
	def __init__(self, probability: Optional[float], usable_for_welcoming: bool = True) -> None:
		self.probability = probability
		self.usable_for_welcoming = usable_for_welcoming

	@staticmethod
	def get_all_prompts() -> 'List[Union[TextPrompt, AudiofilePrompt]]':
		now = datetime.datetime.now()
		if now.month != 1:
			lower_limit = datetime.datetime(year = now.year, month = 12, day = 20)
		else:
			lower_limit = datetime.datetime(year = now.year - 1, month = 12, day = 20)
		if now.month != 12:
			upper_limit = datetime.datetime(year = now.year, month = 1, day = 7)
		else:
			upper_limit = datetime.datetime(year = now.year + 1, month = 1, day = 7)

		return_prompts: 'List[Union[TextPrompt, AudiofilePrompt]]' = []
		if lower_limit <= now <= upper_limit:
			return_prompts += [
				TextPrompt('Merry Christmas, {}'),
				TextPrompt('Look what I found under the Christmas tree, {}'),
				TextPrompt('A nice gift called {}'),
				TextPrompt('They wrapped {} up good'),
				TextPrompt('{} is here to ruin the holidays for everyone'),
				TextPrompt("Wait, {}, that's not milk"),
				TextPrompt('Happy holidays, {}'),
				TextPrompt('The nude deer are coming, {}'),
				TextPrompt('I got {} something festive'),
			]
		else:
			return_prompts += [
				TextPrompt("I'm watching you, {}", usable_for_welcoming = False),
				TextPrompt('Hey, {}, hi!', usable_for_welcoming = False),
				TextPrompt('Boop, {}', usable_for_welcoming = False),
				TextPrompt('{} just joined', usable_for_welcoming = False),
				TextPrompt('Put that cookie down, {}!', usable_for_welcoming = False),
				TextPrompt('Howdy doody {}', usable_for_welcoming = False),
				TextPrompt('Peek-a-boo {}', usable_for_welcoming = False),
				TextPrompt("I'm Batman and you are {}", usable_for_welcoming = False),
				TextPrompt('{}, I like your fursona.', usable_for_welcoming = False),
				TextPrompt('{}? What a cutie!', usable_for_welcoming = False),
				TextPrompt('Move bitches, {} just joined.', usable_for_welcoming = False),
				TextPrompt('Oh no, {} is slow or something.', usable_for_welcoming = False),
				TextPrompt('We love you {}', usable_for_welcoming = False),
				TextPrompt('Cutie {} just joined, be nice to them.', usable_for_welcoming = False),
				TextPrompt('{}, you gonna get yiffed!', usable_for_welcoming = False),
				TextPrompt('After 12 hours of yiff, {} is finally back.', usable_for_welcoming = False),
				TextPrompt('{}, would you like a sedative?', usable_for_welcoming = False),
				TextPrompt("I missed the part where that's my problem, {}", usable_for_welcoming = False),
			]

		return_prompts += [
			AudiofilePrompt(filename.split('/')[-1].split('.wav')[0])
			for filename in os.listdir('audio_files')
			if filename.endswith('.wav')
		]

		return return_prompts

	@staticmethod
	def get_seed_prompts() -> List[str]:
		all_prompts = Prompt.get_all_prompts()
		text_prompts: List[str] = [prompt.prompt for prompt in all_prompts if isinstance(prompt, TextPrompt)]
		return text_prompts

	@staticmethod
	def get_usable_prompts() -> 'List[Union[TextPrompt, AudiofilePrompt]]':
		return [prompt for prompt in Prompt.get_all_prompts() if prompt.usable_for_welcoming]

	@property
	@abstractmethod
	def safe_name(self) -> str:
		pass

	@staticmethod
	def _sanitize_name(unsanitized: str) -> str:
		name = unsanitized.lower()
		name = ''.join([i if (48 <= ord(i) <= 57) or (97 <= ord(i) <= 122) else '_' for i in name])
		return name

	@staticmethod
	def get_random(
		possibilities: 'List[Prompt]',
		random_provider: freem_bots.random_provider.RandomProvider,
	) -> 'Prompt':
		''' Select from an unweighted set '''
		return random_provider.choose_randomly(possibilities)

	@staticmethod
	def get_random_weighted(
		possibilities: 'List[Union[TextPrompt, AudiofilePrompt]]',
		random_provider: freem_bots.random_provider.RandomProvider,
	) -> 'Union[TextPrompt, AudiofilePrompt]':
		''' Select from a weighted set '''

		probability_matrix = []
		subtotal = 0.0
		for possibility in possibilities:
			probability_matrix.append(subtotal)
			if possibility.probability is not None:
				subtotal += possibility.probability
		maximum_value = probability_matrix[-1] + (
			0 if possibilities[-1].probability is None else possibilities[-1].probability
		)
		random_value = random_provider.get_float() * maximum_value
		hit_index = 0
		for i, current_value in enumerate(probability_matrix):
			next_value = maximum_value if i == (len(probability_matrix) - 1) else probability_matrix[i + 1]
			current_value = probability_matrix[i]
			if current_value <= random_value <= next_value:
				hit_index = i
				break
		selected = possibilities[hit_index]
		return selected


class TextPrompt(Prompt):
	def __init__(
		self,
		prompt: str,
		probability: Optional[float] = None,
		usable_for_welcoming: bool = True,
	) -> None:
		self.prompt = prompt
		super().__init__(probability, usable_for_welcoming)

	@property
	def safe_name(self) -> str:
		return Prompt._sanitize_name(self.prompt)

	def __str__(self) -> str:
		return self.prompt


class AudiofilePrompt(Prompt):
	def __init__(
		self,
		filename: str,
		probability: Optional[float] = None,
		usable_for_welcoming: bool = True,
	) -> None:
		self.filename = filename
		super().__init__(probability, usable_for_welcoming)

	@property
	def safe_name(self) -> str:
		return Prompt._sanitize_name(self.filename)

	def __str__(self) -> str:
		return self.filename
