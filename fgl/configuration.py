import typing

import freem_bots
import pedalboard


class FGLConfiguration(freem_bots.Configuration):
	def __init__(self) -> None:
		super().__init__()
		self.openai_token = self._get_string('OPENAI_API_KEY')
		self.redis_hostname = self._get_string('REDIS_HOSTNAME', default_value = 'redis', can_be_none = False)
		self.testing_mode = self._get_bool('TESTING_MODE', default_value = False, can_be_none = True)
		self.enable_slash_commands = self._get_bool(
			'ENABLE_SLASH_COMMANDS',
			default_value = True,
			can_be_none = True,
		)
		self.voice_effects = typing.cast(
			typing.Optional[list[pedalboard.Plugin]],
			[
				pedalboard.PitchShift(
					0.5
				),
			],
		)
